#include <iostream>

using namespace std;

class Node {
    public:
        int data;
        Node* next;
        int get_data();
};

int Node::get_data() {
    return data;
}

class SinglyLinkedList {
    private:
        Node* head = NULL;
    public:
        void append_node(int node_data);
        void print_singly_linkedlist();
};

void SinglyLinkedList::append_node(int node_data){
    Node* new_node_ptr = new Node();
    new_node_ptr->data = node_data;
    new_node_ptr->next = NULL;
    if (head == NULL) {
        this->head = new_node_ptr;
    }
    else {
        Node* node_ptr = new Node();
        node_ptr = this->head;
        while (node_ptr->next != NULL) {
            node_ptr = node_ptr->next;
        }
        node_ptr->next = new_node_ptr;
    }
}

void SinglyLinkedList::print_singly_linkedlist() {
    Node* node_ptr;
    node_ptr = head;
    while (node_ptr != NULL) {
        cout << node_ptr->data << " -> ";
        node_ptr = node_ptr->next;
    }
    cout << endl;
}

int main () {

    int singly_linkedlist_length;
    cin >> singly_linkedlist_length;

    SinglyLinkedList sll;

    int node_data;
    for (int i = 0; i < singly_linkedlist_length; i++) {
        cin >> node_data;
        sll.append_node(node_data);
    }

    sll.print_singly_linkedlist();
    return 0;
}